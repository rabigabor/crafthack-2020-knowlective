// See https://github.com/dialogflow/dialogflow-fulfillment-nodejs
// for Dialogflow fulfillment library docs, samples, and to report issues
'use strict';

const functions = require('firebase-functions');
const { WebhookClient } = require('dialogflow-fulfillment');
const { Card, Suggestion } = require('dialogflow-fulfillment');
const neo4j = require('neo4j-driver');

const uri = 'neo4j+s://f9b41c5c.databases.neo4j.io';
const user = '<USER>';
const password = '<PASSWORD>';

const driver = neo4j.driver(uri, neo4j.auth.basic(user, password));

process.env.DEBUG = 'dialogflow:debug'; // enables lib debugging statements

exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
    const agent = new WebhookClient({ request, response });
    console.log('Dialogflow Request headers: ' + JSON.stringify(request.headers));
    console.log('Dialogflow Request body: ' + JSON.stringify(request.body));
    console.log("YOLOKA");


    async function skillQuery(agent) {
        const session = driver.session();
        const readQuery = `MATCH (skill:SKILL {name:"${agent.parameters.skill}"})<-[:HAS_SKILL]-(employee) RETURN employee`;
        await session.readTransaction(tx => tx.run(readQuery)).then(x => {
            try {
                console.log("records: " + JSON.stringify(x.records));
                if (x.records.length == 0) {
                    agent.add("Unfortunately, nobody yet.");
                } else {
                    const answer = x.records.map(record => {
                        console.log("record: " + JSON.stringify(record));
                        let emp = record.get("employee");
                        return `${emp.properties.name} (${emp.properties.position})`;
                    }).join(", ");
                    agent.add(`These people are quite experienced in ${agent.parameters.skill}: ${answer}.`);
                    // agent.add(`Based on their profile I recommend the following employees: ${answer}.`);
                }
            } catch (e) {
                console.log("ERROR: " + JSON.stringify(e));
            }
        });
    }

    async function projectSkillQuery(agent) {
        const session = driver.session();
        const readQuery = `MATCH (skill:SKILL {name:"${agent.parameters.skill}"})<-[:NEEDS_SKILL]-(project:PROJECT)-[:ORDERED_BY]->(customer:CUSTOMER) RETURN project, customer`;
        const x = await session.readTransaction(tx => tx.run(readQuery));
        try {
            if (x.records.length == 0) {
                agent.add("No, we don’t have relevant experience, but you can search for relevant skills to get one.");
            } else {
                const answer = x.records.map(record => {
                    let project = record.get("project");
                    let customer = record.get("customer");
                    const subAnswer = `${project.properties.name} (${customer.properties.name})`;
                    return subAnswer;
                }).join(", ");
                agent.add(`Yes, we have great references related to ${agent.parameters.skill}: ${answer}.`);
                //agent.add(`Yes, we used ${agent.parameters.skill} in the following projects: ${answer}.`);
            }
        } catch (e) {
            console.log("ERROR: " + JSON.stringify(e));
        }
    }

    async function projectCustomerQuery(agent) {
        const session = driver.session();
        const readQuery = `MATCH (project: PROJECT)-[:ORDERED_BY]->(customer {industry:'${agent.parameters.industry}'}) RETURN customer, project`;
        const x = await session.readTransaction(tx => tx.run(readQuery));
        try {
            if (x.records.length == 0) {
                agent.add("No, we don’t have experience in that industry, but you can search for relevant skills or projects from another industry.");
            } else {
                const answer = x.records.map(record => {
                    let project = record.get("project");
                    let customer = record.get("customer");
                    const subAnswer = `${project.properties.name} (${customer.properties.name})`;
                    return subAnswer;
                }).join(", ");
                agent.add(`Yes, we have great experiences in the ${agent.parameters.industry} industry by the following projects: ${answer}`);
                //agent.add(`Yes, we used ${agent.parameters.skill} in the following projects: ${answer}.`);
            }
        } catch (e) {
            console.log("ERROR: " + JSON.stringify(e));
        }
    }
    async function projectEmployeeQuery(agent) {
        const session = driver.session();
        const readQuery = `MATCH (employee:EMPLOYEE)-[:PARTICIPATES_IN {role:'${agent.parameters.roleinproject}'}]->(project:PROJECT)-[:ORDERED_BY]->(customer:CUSTOMER {name:'${agent.parameters.customer}'}) RETURN employee`;
        const x = await session.readTransaction(tx => tx.run(readQuery));
        try {
            if (x.records.length == 0) {
                agent.add("Unfortunately, I don't have any information about this. :(");
            } else {
                const answer = x.records.map(record => {
                    let employee = record.get("employee");
                    const subAnswer = `${employee.properties.name}`;
                    return subAnswer;
                }).join(", ");
                agent.add(`The ${agent.parameters.roleinproject} in the project by ${agent.parameters.customer}: ${answer}`);
                //agent.add(`Yes, we used ${agent.parameters.skill} in the following projects: ${answer}.`);
            }
        } catch (e) {
            console.log("ERROR: " + JSON.stringify(e));
        }
    }

    async function customerRegionQuery(agent) {
        const session = driver.session();
        const readQuery = `MATCH (cust:CUSTOMER {region:'${agent.parameters.region}'}) RETURN cust`;
        const x = await session.readTransaction(tx => tx.run(readQuery));
        try {
            if (x.records.length == 0) {
                agent.add("No, we don't have any clients from there.");
            } else {
                const answer = x.records.map(record => {
                    let customer = record.get("cust");
                    const subAnswer = `${customer.properties.name}`;
                    return subAnswer;
                }).join(", ");
                agent.add(`Yes, we have clients from ${agent.parameters.region}: ${answer}`);
                //agent.add(`Yes, we used ${agent.parameters.skill} in the following projects: ${answer}.`);
            }
        } catch (e) {
            console.log("ERROR: " + JSON.stringify(e));
        }
    }

    async function employeeRoleQuery(agent) {
        const session = driver.session();
        const readQuery = `MATCH (emp:EMPLOYEE)-[:PARTICIPATES_IN {role:'${agent.parameters.roleinproject}'}]->(project:PROJECT) RETURN emp, project`;
        const x = await session.readTransaction(tx => tx.run(readQuery));
        try {
            if (x.records.length == 0) {
                agent.add("Unfortunately, we don't have any experience in that yet.");
            } else {
                const answer = x.records.map(record => {
                    let employee = record.get("emp");
                    const subAnswer = `${employee.properties.name}`;
                    return subAnswer;
                }).join(", ");
                agent.add(`The following employees are experienced in ${agent.parameters.roleinproject}: ${answer}`);
            }
        } catch (e) {
            console.log("ERROR: " + JSON.stringify(e));
        }
    }

    // Run the proper function handler based on the matched Dialogflow intent name
    let intentMap = new Map();
    intentMap.set('Skill Query', skillQuery);
    intentMap.set('ProjectSkillQuery', projectSkillQuery);
    intentMap.set('ProjectCustomerQuery', projectCustomerQuery);
    intentMap.set('ProjectEmployeeQuery', projectEmployeeQuery);
    intentMap.set('CustomerRegionQuery', customerRegionQuery);
    intentMap.set('EmployeeRoleQuery', employeeRoleQuery);
    // intentMap.set('your intent name here', yourFunctionHandler);
    // intentMap.set('your intent name here', googleAssistantHandler);
    agent.handleRequest(intentMap);
});
