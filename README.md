## Description
During the everyday life of professional service companies, there are constantly changing projects and partner organizations. This generates a significant amount of knowledge and information, which could be a real game changer if the organization could use the data effectively.
It may sound easy, but if you ever worked for a company like that you know how it works. You try to find information in the maze of folders or ask around the company whether somebody knows someone who participated in a project like yours.

That is why the goal of KnowLective is to solve this time consuming problem and increase efficiency inside the organization.

To overcome these issues,  we’ve created a graph based knowledge model which consists of four main entities:
* the customers or clients of the company,
* the projects ordered by these clients,
* the employees who are working on these projects,
* and of course the skills which are needed for a specific project or used by the employees.

The speciality of this type of storing is that we can define the relationship between the objects. This is important because we can profile really complex organizations in a simple way.

To make it more user friendly, we’ve built a chatbot based on the graph model to help gain relevant information for all employees with different technical and business backgrounds inside the organization. The KnowLective chatbot uses natural language processing algorithms, which can identify the keywords inside the questions and helps users to select the necessary informations.

## Usage notes
At the Demo URL, you can try out our MVP, which can handle several scenarios successfully. For your help, we created two screenshots about the filtered knowledge graph to know what kind of information might be interesting to query. 

## Main topics that you can ask about
* Employees based on skill (Who has experience in HTML?)
* Previous/current projects based on skill (Do we have any project experience with Python?)
* Employees based on project roles with a specific client (Who was the tech lead on the project with Sinopec?)
* Clients based on region (Do we have any clients in Europe?)
* Projects based on industry(Do we have any experience in the healthcare industry?)

## Entities
### Skills
* HTML
* CSS
* JavaScript
* Angular
* Java
* C++
* Node.js
* Python
* PHP
* SQL
* R
* PowerBI
* Project Management

### Projects
* Customer segmentation
* ERP system
* Production optimalization
* Churn analysis

### Customers
* AirAsia
* Sinopec
* MOL
* Janssen

### Other
* *Regions:* Asian-Pacific, Europe, USA
* *Industries:* Healthcare, Airline, Oil & Gas

## Used technologies
* DialogFlow for the chatbot
* Neo4j for the knowledge graph
* Google Cloud Functions for the integration

## Note
The product is only a proof-of-concept. Of course, it can be developed further in several dimensions, like more training examples, broader choice of questions, more entities, etc.
